/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */

// JavaScript source code
angular.module('la_rockola',[])
.controller('filasUsuarios',function($scope,$http){
    
    $scope.nombre_usuario = "";
    $scope.apellido_usuario = "";
    $scope.email = "";
    $scope.contraseña = "";
    $scope.rol = "";

    
    // accion del boton consultar
    $scope.consultar = function(){
        if($scope.cedula == undefined || $scope.cedula == null){
            $scope.cedula = 0;
        }
        
        $http.get("/la_rockol/usuario?cedula="+$scope.cedula).then(function(data){
            console.log(data.data);
            $scope.nombre_usuario = data.data.nombre_usuario;
            $scope.apellido_usuario = data.data.apellido_usuario;
            $scope.email = data.data.email;
            $scope.contraseña = data.data.contraseña;
            $scope.rol = data.data.rol;
        },function(){
             //error
            $scope.nombre_usuario = "";
            $scope.apellido_usuario = "";
            $scope.email = "";
            $scope.contraseña = "";
            $scope.contraseña = "";
            $scope.rol = "";            
            $scope.filas = []; // guarda
        });
        
        $http.get("/la_rockola/usuario?cedula="+$scope.cedula).then(function(data){
            console.log(data.data);
            $scope.filas = data.data;  
        },function(){
            $scope.filas = [];
        });
    };
    
    //acción del botón actualizar
    $scope.actualizar = function(){
        if($scope.cancion == undefined || $scope.cancion == null){
            $scope.cancion = 0;
        }
        data = {
            "id_cancion": $scope.cancion,
            "nombre_cancion":$scope.cancion,
            "artista": $scope.cancion,
            "genero": $scope.cancion,
            "album": $scope.cancion
        }
        $http.post('/la_rockola/canciones', data).then(function(data){
            //success
            $scope.id_cancion = data.data.id_cancion;
            $scope.nombre_cancion = data.data.nombre_cancion;
            $scope.artista = data.data.artista;
            $scope.genero = data.data.genero;
            $scope.albunm = data.data.albunm;
        },function(){
            //error
            $scope.id_cancion = "";
            $scope.nombre_cancion = "";
            $scope.artista = "";
            $scope.genero = "";
            $scope.albunm = "";
            $scope.filas = [];
        });
        
    };
    
   /* //acción del botón actualizar
    $scope.guardar = function(){
        data = {
            "nombre":$scope.nombre,
            "edad": $scope.edad,
            "correo": $scope.correo
        };
        $http.post('/cajero/estudiante', data).then(function(data){
            //success
            $scope.nombre = data.data.nombre;
            $scope.edad = data.data.edad;
            $scope.correo = data.data.correo;
        },function(){
            //error
            $scope.nombre = "";
            $scope.edad = "";
            $scope.correo = "";
            $scope.filas = [];
        });
        
    };*/
    
    
    // acción del botón borrar
    $scope.borrar = function(){
        if($scope.id_cancion == undefined || $scope.id_cancion == null){
            $scope.id_cancion = 0;
        }
        data = {
            "id_cancion": $scope.id_cancion
        }
        $http.delete('/la_rockoa/eliminarcancion/'+$scope.id_cancion, data).then(function(data){
            alert("La cuenta ha sido eliminada");
            
        },function(){
            alert("Ha ocurrido un error");
        });
    };
});


