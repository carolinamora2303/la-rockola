package com.example.la_rockola;

import com.example.la_rockola.modelos.Album;
import com.example.la_rockola.modelos.Artista;
import com.example.la_rockola.modelos.Canciones;
import com.example.la_rockola.modelos.Generos_musicales;
import com.example.la_rockola.modelos.Usuario;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@SpringBootApplication
@RestController
public class LaRockolaApplication {

    @Autowired(required = true)
    Canciones c;
    @Autowired(required = true)
    Usuario u;
    @Autowired(required = true)
    Artista a;
    @Autowired(required = true)
    Album al;
    @Autowired(required = true)
    Generos_musicales g;

    public static void main(String[] args) {
        SpringApplication.run(LaRockolaApplication.class, args);
    }

    @GetMapping("/Usuario")
    public String consultarUsuarioPorCedula(@RequestParam(value = "cedula", defaultValue = "1") int cedula) throws ClassNotFoundException, SQLException {
        u.setId_usuario(cedula);
        if (u.consultar()) {
            String res = new Gson().toJson(u);
            u.setId_usuario(0);
            u.setNombre_usuario("");
            u.setApellido_usuario("");
            u.setEmail("");
            u.setContraseña("");
            u.setRol("");
            return res;
        } else {
            return new Gson().toJson(u);
        }
    }
    
    @GetMapping("/Canciones")
    public String consultarCancion(@RequestParam(value = "id", defaultValue = "1") int id) throws ClassNotFoundException, SQLException{
           List<Canciones> cancion = c.consultarTodo(id);
           if(!cancion.isEmpty()){
                return new Gson().toJson(cancion);
           }
           else{
            return new Gson().toJson(cancion);
           }
    }
    
    @PostMapping(path = "/Canciones",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    
    @GetMapping("/Cancioes")
    public String actualizarCancion(@RequestBody String cancion) throws ClassNotFoundException, SQLException{
        Canciones f = new Gson().fromJson(cancion, Canciones.class);
        c.setId_cancion(f.getId_cancion());
        c.setNombre_cancion(f.getNombre_cancion());
        c.setArtista(f.getArtista());
        c.setAlbum(f.getAlbum());
        c.setGenero(f.getGenero());
        c.setAnno(f.getAnno());
        c.actualizar();
        return new Gson().toJson(c);
    }
    
    @PostMapping(path = "/Usuario",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    
    @GetMapping("/Usuario")
    public String actualizarUsuario(@RequestBody String usuario) throws ClassNotFoundException, SQLException{
        Usuario f = new Gson().fromJson(usuario, Usuario.class);
        u.setId_usuario(f.getId_usuario());
        u.setNombre_usuario(f.getNombre_usuario());
        u.setApellido_usuario(f.getApellido_usuario());
        u.setEmail(f.getEmail());
        u.setContraseña(f.getContraseña());
        u.setRol(f.getRol());
        c.actualizar();
        
        return new Gson().toJson(u);
    }
    
    @PostMapping(path = "/Artista",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    
    @GetMapping("/Artista")
    public String actualizarGeneroMusicalDeArtista(@RequestBody String artista) throws ClassNotFoundException, SQLException{
        Artista f = new Gson().fromJson(artista, Artista.class);
        a.setGenero_musical(f.getGenero_musical());
        a.actualizarGenero();
        
        return new Gson().toJson(a);
    }
    
    @DeleteMapping("/eliminarcancion/{id}")
    public String borrarCancion(@PathVariable ("id") int id) throws ClassNotFoundException, SQLException{
        c.setId_cancion(id);
        c.borrar();
        
        return "Los datos del id indicado han sido borrados";
    }
    
    @DeleteMapping("/eliminaralbum/{id} ")
    public String borrarAlbum(@PathVariable ("id") int id) throws ClassNotFoundException, SQLException{
        al.setId_album(id);
        al.borrar();
        
        return "Los datos del id indicado han sido borrados";
    }
}

    

