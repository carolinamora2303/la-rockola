/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.la_rockola.modelos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author camilo
 */
@Component("Usuario")
public class Usuario {

    @Autowired
    transient JdbcTemplate jdbcTemplate;

    private int id_usuario;

    private String nombre_usuario, apellido_usuario, cedula, email, contraseña, rol;

    //constructor
    public Usuario(int id_usuario, String nombre_usuario, String apellido_usuario, String cedula, String email, String contraseña, String rol) {
        this.id_usuario = id_usuario;
        this.nombre_usuario = nombre_usuario;
        this.apellido_usuario = apellido_usuario;
        this.cedula = cedula;
        this.email = email;
        this.contraseña = contraseña;
        this.rol = rol;
    }

    //metodos
    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getNombre_usuario() {
        return nombre_usuario;
    }

    public void setNombre_usuario(String nombre_usuario) {
        this.nombre_usuario = nombre_usuario;
    }

    public String getApellido_usuario() {
        return apellido_usuario;
    }

    public void setApellido_usuario(String apellido_usuario) {
        this.apellido_usuario = apellido_usuario;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    @Override
    public String toString() {
        return "Usuario{" + ", id_usuario=" + id_usuario + ", nombre_usuario=" + nombre_usuario + ", apellido_usuario=" + apellido_usuario + ", cedula=" + cedula + ", email=" + email + ", contrase\u00f1a=" + contraseña + ", rol=" + rol + '}';
    }
    //

    // CRUD - C
    public String guardar() throws ClassNotFoundException, SQLException {
        String sql = "INSERT INTO Usuario(id_usuario,nombre_usuario, apellido_usuario, cedula, email, contraseña, rol )"
                + "VALUES (?,?,?,?,?,?,?)";
        return sql;
    }

    //CRUD - R
    public boolean consultar() throws ClassNotFoundException, SQLException {
        String sql = "SELECT id_usuario,nombre_usuario, apellido_usuario, cedula, email, contraseña, rol FROM Usuario "
                + "WHERE id_usuario = ?";
        List<Usuario> usuarios = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Usuario(
                        rs.getInt("id_usuario"),
                        rs.getString("nombre_usuario"),
                        rs.getString("apellido_usuario"),
                        rs.getString("cedula"),
                        rs.getString("email"),
                        rs.getString("contraseña"),
                        rs.getString("rol")
                ), new Object[]{this.getId_usuario()});
        if (usuarios != null && !usuarios.isEmpty()) {
            this.setId_usuario(usuarios.get(0).getId_usuario());
            this.setNombre_usuario(usuarios.get(0).getNombre_usuario());
            this.setApellido_usuario(usuarios.get(0).getApellido_usuario());
            this.setCedula(usuarios.get(0).getCedula());
            this.setEmail(usuarios.get(0).getEmail());
            this.setContraseña(usuarios.get(0).getContraseña());
            this.setRol(usuarios.get(0).getRol());
            return true;
        } else {
            return false;
        }
    }

    //CRUD - U
    public void actualizar() throws ClassNotFoundException, SQLException {
        String sql = "UPDATE Usuario SET nombre_usuario = ?"
                + "', apellido_usuario = ?"
                //+ "', cedula = '" + cedula
                + "', email =  ?"
                + "', contraseña = ?"
                + "', rol = ?"
                + "WHERE id_usuario = ?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setString(2, this.getNombre_usuario());
        ps.setString(3, this.getApellido_usuario());
        //ps.setString(4, this.getCedula());
        ps.setString(5, this.getEmail());
        ps.setString(6, this.getContraseña());
        ps.setString(7, this.getRol());
        ps.executeUpdate();
        ps.close();
    }

    //CRUD - D
    /*public boolean borrar() throws ClassNotFoundException, SQLException{
        String sql = "DELETE FROM Usuaio WHERE Id_usuario = '"+ id_usuario + "'" ;
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setInt(1, this.getId_usuario());
        ps.execute();
        ps.close();
        
        return true;
    } */
}
