/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.la_rockola.modelos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author saraycamilo
 */
public class Artista {

    //Paquete consultas
    @Autowired
    transient JdbcTemplate jdbcTemplate;

    //atributos
    private int Id_artista; //llave
    private String nombre_artista;
    private String pais_artista;
    private String genero_musical;

    //constructor
    public Artista() {
    }

    public Artista(int Id_artista, String nombre_artista, String pais_artista, String genero_musical) {
        this.Id_artista = Id_artista;
        this.nombre_artista = nombre_artista;
        this.pais_artista = pais_artista;
        this.genero_musical = genero_musical;
    }

    //metodos
    public int getId_artista() {
        return Id_artista;
    }

    public void setId_artista(int Id_artista) {
        this.Id_artista = Id_artista;
    }

    public String getNombre_artista() {
        return nombre_artista;
    }

    public void setNombre_artista(String nombre_artista) {
        this.nombre_artista = nombre_artista;
    }

    public String getPais_artista() {
        return pais_artista;
    }

    public void setPais_artista(String pais_artista) {
        this.pais_artista = pais_artista;
    }

    public String getGenero_musical() {
        return genero_musical;
    }

    public void setGenero_musical(String genero_musical) {
        this.genero_musical = genero_musical;
    }

    // CRUD -C
    public String guardar() throws ClassNotFoundException, SQLException {
        String sql = "INSERT INTO artista (id_artista, nombre_artista, pais_artista, genero_musical) VALUES(?,?,?,?)";
        return sql;
    }

    //CRUD -R
    public boolean consultar() throws ClassNotFoundException, SQLException {
        String sql = "SELECT id_artista, nombre_artista, pais_artista, genero_musical FROM artista WHERE id_artista = ?";
        List<Artista> artistas = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Artista(
                        rs.getInt("Id_artista"),
                        rs.getString("nombre_artista"),
                        rs.getString("pais_artista"),
                        rs.getString("genero_musical")
                ), new Object[]{this.getId_artista()});

        if (artistas != null && !artistas.isEmpty()) {
            this.setId_artista(artistas.get(0).getId_artista());
            this.setNombre_artista(artistas.get(0).getNombre_artista());
            this.setPais_artista(artistas.get(0).getPais_artista());
            this.setGenero_musical(artistas.get(0).getGenero_musical());

            return true;
        } else {
            return false;
        }
    }

    public List<Artista> consultarTodo() throws ClassNotFoundException, SQLException {
        String sql = "SELECT id_artista, nombre_artista, pais_artista, genero_musical FROM artista WHERE id_artista = ?";
        List<Artista> artistas = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Artista(
                        rs.getInt("Id_artista"),
                        rs.getString("nombre_artista"),
                        rs.getString("pais_artista"),
                        rs.getString("genero_musical")
                ), new Object[]{this.getId_artista()});
        return artistas;
    }

    //CRUD -U
    public String actualizarGenero() throws ClassNotFoundException, SQLException {
        String sql = "UPDATE artista SET genero_musical=?  WHERE Id_artista =?";
        return sql;
    }
    

    //CRUD -D
    public boolean borrar() throws ClassNotFoundException, SQLException {
        String sql = "DELETE FROM artista WHERE Id_artista = ?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setInt(1, this.getId_artista());
        ps.execute();
        ps.close();
        return true;
    }
}
