/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.la_rockola.modelos;

import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author camilo
 */
public class Generos_musicales {
    //Atributos

    @Autowired
    transient JdbcTemplate jdbcTemplate;
    private int id_genero;
    private String nombre_genero;

    //Contructor
    public Generos_musicales(int id_genero, String nombre_genero) {
        this.id_genero = id_genero;
        this.nombre_genero = nombre_genero;

    }

    public Generos_musicales() {
    }

    //Metodos/funciones
    public int getId_genero() {
        return id_genero;
    }

    public void setId_genero(int id_genero) {
        this.id_genero = id_genero;
    }

    public String getNombre_genero() {
        return nombre_genero;
    }

    public void setNombre_genero(String nombre_genero) {
        this.nombre_genero = nombre_genero;
    }

    //CRUD - C
    public String guardar() throws ClassNotFoundException, SQLException {
        String sql = "INSERT INTO generos_musicales(id_genero,nombre_genero,)VALUES(" + id_genero + "," + nombre_genero + ")";
        return sql;

    }

    // CRUD - R
    public boolean consultar() throws ClassNotFoundException, SQLException {
        String sql = "SELEC id_genero,nombre_genero FROM generos_musicales WHERE id_genero = ?";

        List<Generos_musicales> genero = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Generos_musicales(
                        rs.getInt("id_genero"), // => 123
                        rs.getString("nombre_genero") // => Julia
                ), new Object[]{this.getId_genero()});
        if (genero != null && !genero.isEmpty()) {
            this.setId_genero(genero.get(0).getId_genero());
            this.setNombre_genero(genero.get(0).getNombre_genero());
            return true;
        } else {
            return false;
        }
    }

    public List<Generos_musicales> consultarTodo() throws ClassNotFoundException, SQLException {
        String sql = "SELEC id_genero,nombre_genero FROM generos_musicales WHERE id_genero = ?";
        List<Generos_musicales> genero = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Generos_musicales(
                        rs.getInt("id_genero"), // => 123
                        rs.getString("nombre_genero") // => Julia
                ), new Object[]{this.getId_genero()});

        return genero;
    }

    public String actualizar() throws ClassNotFoundException, SQLException {
        String sql = "DELETE FROM generos_musicales WHERE id_generos = ?";
        return sql;
    }

}
