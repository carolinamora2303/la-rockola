/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.la_rockola.modelos;

/**
 *
 * @author jimmy
 */
// Import 
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;
import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author jimmy
 */
@Component("canciones")
public class Canciones {

    //Paquetes necesarios
    @Autowired
    transient JdbcTemplate jdbcTemplate;//palabra para serializarse 

    //Atributos
    private int id_cancion;
    private String nombre_cancion;
    private String artista;
    private String genero;
    private String album;
    private String anno;
    
    private Artista id_artista;//llave foranea 
    private Album id_album;//llave foranea 

    private Generos_musicales id_genero;//llave foranea

    private Usuario id_Usuario;//llave foranea 

    //Constructor 
    public Canciones(int id_cancion, String nombre_cancion, String artista, String genero, String album, String anno) {
        super();
        this.id_cancion = id_cancion;
        this.nombre_cancion = nombre_cancion;
        this.artista = artista;
        this.genero = genero;
        this.album = album;
        this.anno = anno;
    }
    
    public Canciones() {
    }
    
    public int getId_cancion() {
        return id_cancion;
    }
    
    public void setId_cancion(int id_cancion) {
        this.id_cancion = id_cancion;
    }
    
    public String getNombre_cancion() {
        return nombre_cancion;
    }
    
    public void setNombre_cancion(String nombre_cancion) {
        this.nombre_cancion = nombre_cancion;
    }
    
    public String getArtista() {
        return artista;
    }
    
    public void setArtista(String artista) {
        this.artista = artista;
    }
    
    public String getGenero() {
        return genero;
    }
    
    public void setGenero(String genero) {
        this.genero = genero;
    }
    
    public String getAnno() {
        return anno;
    }
    
    public void setAnno(String anno) {
        this.anno = anno;
    }
    
    public String getAlbum() {
        return album;
    }
    
    public void setAlbum(String album) {
        this.album = album;
    }
    
    public int getId_artista() {
        return id_artista.getId_artista();
    }
    
    public void setId_artista(int id_artista) {
        this.id_artista.setId_artista(id_artista);
    }
    
    public int getId_album() {
        return id_album.getId_album();
    }
    
    public void setId_album(int id_album) {
        this.id_album.setId_album(id_album);        
    }
    
    public int getId_genero() {
        return id_genero.getId_genero();
    }
    
    public void setId_genero(int id_genero) {
        this.id_genero.setId_genero(id_genero);
    }
    
    public float getId_Usuario() {
        return id_Usuario.getId_usuario();
    }
    
    public void setId_Usuario(int id_Usuario) {
        this.id_Usuario.setId_usuario(id_Usuario);
    }
    
    @Override
    public String toString() {
        return "Canciones{" + "id_cancion=" + id_cancion + ", nombreCancion=" + nombre_cancion + ", artista=" + artista + ", genero=" + genero + ", anno=" + anno + ", id_artista=" + id_artista + ", id_album=" + id_album + ", id_genero_musical=" + id_genero + ", id_Usuario=" + id_Usuario + '}';
    }

    //CRUD- C
    public String guardar() throws ClassNotFoundException, SQLException {
        String sql = "INSERT INTO canciones(id_cancion, nombre_cancion,artista,genero,album,anno)"
                + "VALUES (?,?,?,?,?,? )";
        return sql;
    }

    //CRUD -R
    public boolean consultar() throws ClassNotFoundException, SQLException {
        String sql = "SELECT id_cancion, nombre_cancion,artista,genero,album,anno FROM canciones WHERE id_genero = ?";
        List<Canciones> canciones = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Canciones(
                        rs.getInt(id_cancion),
                        rs.getString(nombre_cancion),
                        rs.getString(artista),
                        rs.getString(genero),
                        rs.getString(album),
                        rs.getString(anno)
                ), new Object[]{this.getId_genero()});
        
        if (canciones != null && !canciones.isEmpty()) {
            this.setId_cancion(canciones.get(0).getId_cancion());
            this.setNombre_cancion(canciones.get(0).getNombre_cancion());
            this.setArtista(canciones.get(0).getArtista());
            this.setGenero(canciones.get(0).getGenero());
            this.setAlbum(canciones.get(0).getAlbum());
            this.setAnno(canciones.get(0).getAnno());
            
            return true;
        } else {
            return false;
        }
        
    }
    
    public List<Canciones> consultarTodo(int id_cancion) throws ClassNotFoundException, SQLException {
        String sql = "SELECT id_cancion, nombre_cancion,artista,genero,album,anno FROM canciones WHERE id_genero = ?";
        List<Canciones> canciones = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Canciones(
                        rs.getInt(id_cancion),
                        rs.getString(nombre_cancion),
                        rs.getString(artista),
                        rs.getString(genero),
                        rs.getString(album),
                        rs.getString(anno)
                ), new Object[]{this.getId_genero()});
        
        return canciones;
    }

    //CRUD - U
    public String actualizar() throws ClassNotFoundException, SQLException {
        String sql = "UPDATE canciones SET nombre_cancion = ?,artista = ?,genero = ?,album = ?,anno =?  WHERE id_cancion =?";
        return sql;
    }

    //CRUD - D 
    public boolean borrar() throws ClassNotFoundException, SQLException {
        String sql = "DELETE FROM canciones WHERE id_cancion = ?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setInt(1, this.getId_cancion());
        ps.execute();
        ps.close();
        
        return true;
    }
}
