/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.la_rockola.modelos;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
/**
 *
 * @author camilo
 */

public class Album  {
   
    //paquete de ejecucion de consultas sql//
    transient JdbcTemplate jdbcTemplate;
    //atributos//
    
    private int id_album;
    private String nombre_album;
    private int anno;
    //constructor
    public Album(int id_album, String nombre_album, int anno) {
        this.id_album = id_album;
        this.nombre_album = nombre_album;
        this.anno = anno;
    }

    public Album() {
    }
    
    //metodos//

    public int getId_album() {
        return id_album;
    }

    public void setId_album(int id_album) {
        this.id_album = id_album;
    }

    public String getNombre_album() {
        return nombre_album;
    }

    public void setNombre_album(String nombre_album) {
        this.nombre_album = nombre_album;
    }

    public int getAnno() {
        return anno;
    }

    public void setAnno(int anno) {
        this.anno = anno;
    }
    //crud //
    public String guardar() throws ClassNotFoundException, SQLException{
        String sql = "INSERT INTO album(id_album, nombre_album, anno) VALUES(?,?,?)";
        return sql;
        }
    //crud consulta//
    public boolean consultar() throws ClassNotFoundException, SQLException{
        String sql = "SELECT , id_album, nombre_album, anno FROM album WHERE id_album = ?";
        List<Album> album = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Album(
                        rs.getInt("id_album"),
                        rs.getString("nombre_album"),
                        rs.getInt("anno")
                        
                ), new Object[]{this.getId_album()});
        if(album != null && !album.isEmpty()){
            this.setId_album(album.get(0).getId_album());
            this.setNombre_album(album.get(0).getNombre_album());
            this.setAnno(album.get(0).getAnno());
            return true;
        }
        else{
            return false;
        }
        
       
    }
    
    public List<Album> consultarTodo(int id_album) throws ClassNotFoundException, SQLException {
        String sql = "SELECT , id_album, nombre_album, anno FROM album WHERE id_album = ?";
        List<Album> album = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Album(
                        rs.getInt("id_album"),
                        rs.getString("nombre_album"),
                        rs.getInt("anno")
                        
                ), new Object[]{this.getId_album()});

        return album;
    }
    
    
    //crud -u//
    public String actualizar() throws ClassNotFoundException, SQLException{
        String sql = "UPDATE album SET Nombre_album = ?, Anno = ?, id_album = ?";
        return sql;
    }
   //crud -D//
    public boolean borrar ()throws ClassNotFoundException, SQLException{
        String sql ="DELETE FROM album WHERE id_album = ?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setInt(1, this.getId_album());
        ps.execute();
        ps.close();
        
        return true;
        
    }
}